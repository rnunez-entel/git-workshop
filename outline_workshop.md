1. Disclaimer (0 min)
2. Introducción (10 min)
3. El commit (5 min)
4. Clone, commit, push, fetch, pull (5 min)
5. Set-up (10 min)
6. División de trabajo (10 min)
7. Uniendo las piezas (10 min)
8. Demostración en consola vs SourceTree (0 min)
9. Detalles sobre clone, init y origin (0 min)
10. Testeo (0 min)
11. Ramas (30 min)
12. Buenas Prácticas (20 min)
13. Alias (0 min)
14. Actividad Final(10 min)
15. Saludos! (2 min)
16. Referencias (2 min)

Total: 112/120 minutos








